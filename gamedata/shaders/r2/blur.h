#ifndef BLUR_H
#define BLUR_H
/*
// default: factor = 2
float3 simpleblur(v2p I, float factor)
{
   float2 center=I.tc0;
   float dx = factor * .5f/1024.f;
   float dy = factor * .5f/768.f;
   return
   (
      4 * tex2D( s_image, center ).rgb +
      2 * tex2D( s_image, center + float2(dx, 0) ).rgb +
      2 * tex2D( s_image, center + float2(-dx, 0) ).rgb +
      2 * tex2D( s_image, center + float2(0, dy) ).rgb +
      2 * tex2D( s_image, center + float2(0, -dy) ).rgb +
	  1 * tex2D( s_image, center + float2(dx, dy) ).rgb +
	  1 * tex2D( s_image, center + float2(dx, -dy) ).rgb +
	  1 * tex2D( s_image, center + float2(-dx, dy) ).rgb +
	  1 * tex2D( s_image, center + float2(-dx, -dy) ).rgb
	)/16;
}
*/
float4 gaussblur(float2 center, float factor)
{   
   float dx = factor * .5f/1024.f;
   float dy = factor * .5f/768.f;
   //float dx = factor * 0.00048828125;
   //float dy = factor * 0.00065104167;
   float4 col = 0;
   col.rgb =
   /*
   (	
	  1 * tex2D( s_image, center ).rgb +
      1 * tex2D( s_image, center + float2(dx, 0) ).rgb +    
      1 * tex2D( s_image, center + float2(0, dy) ).rgb +    
	  1 * tex2D( s_image, center + float2(dx, -dy) ).rgb +	
	  1 * tex2D( s_image, center + float2(-dx, -dy) ).rgb
	)/5;
	*/
	(
      1 * tex2D( s_image, center ).rgb +
      1 * tex2D( s_image, center + float2(dx, 0) ).rgb +
	  1 * tex2D( s_image, center + float2(dy, 0) ).rgb +
      1 * tex2D( s_image, center + float2(-dx, 0) ).rgb +
      //1 * tex2D( s_image, center + float2(-dx, 0) ).rgb +
      1 * tex2D( s_image, center + float2(-dx, -dy) ).rgb +
      1 * tex2D( s_image, center + float2(dx, -dy) ).rgb
	)/6;
	return col;
}

float4 simpleblur(v2p I, float factor) { return gaussblur(I.tc0, factor); }

// Classic blur algorithm from Sky4ace
// I: input texcoords
// sum: input color of pixel to process
// factor: blur amount
// samples: blur quality / samples count
// RETURNS: blurred color component (must be added to existing img)
float3 classicblur(float2 cent, float3 sum, float factor, int samples)
{
	float2 bias=float2(.5f/1024.f,.5f/768.f);
	float2 scale = bias * factor;
	float2 o [48];
	o[0]=float2(-0.326212f,-0.405810f)*scale;
	o[1]=float2(-0.840144f,-0.073580f)*scale;
	o[2]=float2(-0.695914f,0.457137f)*scale;
	o[3]=float2(-0.203345f,0.620716f)*scale;
	o[4]=float2(0.962340f,-0.194983f)*scale;
	o[5]=float2(0.473434f,-0.480026f)*scale;
	o[6]=float2(0.519456f,0.767022f)*scale;
	o[7]=float2(0.185461f,-0.893124f)*scale;
	o[8]=float2(0.507431f,0.064425f)*scale;
	o[9]=float2(0.896420f,0.412458f)*scale;
	o[10]=float2(-0.321940f,-0.932615f)*scale;
	o[11]=float2(-0.791559f,-0.597710f)*scale;	
		
	float contrib=1.h;
	int j = 0;
	
	for (int i=0;i<samples;i++)
	{	
		float2 tap=cent+o[j];
		float3 tap_color=tex2D(s_image,tap).rgb;
		sum+=tap_color;
		contrib++;
		j++; if (j > 11) { j=0; }
	}
	
	return sum;
}
#endif // BLUR_H